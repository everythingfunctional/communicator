# Communicator

A Fortran library to enable synchronous, two way communication between images.
This enables communication of objects which are polymorphic or which have allocatable components.
