program hello_world
    use communicator, only: communicator_t

    implicit none
    type(communicator_t) :: comm
    character(len=2) :: image_str
    character(len=:), allocatable :: message
    class(*), allocatable :: payload

    call comm%init()
    if (this_image() == num_images()) then
        write(image_str, '(I2)') this_image()
        message = "Hello, from image " // image_str
        if (this_image() > 1) call comm%send_to(this_image()-1, message)
    else
        call comm%receive_from(this_image()+1, payload)
        select type (payload)
        type is (character(len=*))
            message = payload
        class default
            message = "Didn't get a string message"
        end select
        if (this_image() > 1) call comm%send_to(this_image()-1, message)
    end if
    print *, "On image ", this_image(), " received: ", message
end program